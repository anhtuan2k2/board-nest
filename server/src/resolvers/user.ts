import { validateRegisterInput } from './../utils/validateRegisterInput';
import argon2 from 'argon2'
import { User } from "../entities/User";
import { Arg, Mutation, Resolver } from "type-graphql";
import { UserMutationResponse } from '../types/UserMutationResponse';
import { RegisterInput } from '../types/RegisterInput';
import { LoginInput } from '../types/LoginInput';

@Resolver()
export class UserResolver {
    @Mutation(_returns => UserMutationResponse, { nullable: true })
    async register(
        @Arg('registerInput') registerInput: RegisterInput
    ): Promise<UserMutationResponse> {

        const validateRegisterInputErros = validateRegisterInput(registerInput)
        if (validateRegisterInputErros !== null) {
            return { code: 400, success: false, ...validateRegisterInputErros }
        }
        try {
            const { username, email, password } = registerInput
            const existingUser = await User.findOne({ where: [{ username }, { email }] })
            let valueInvalid = existingUser?.username === username ? 'username' : 'email';
            if (existingUser) return {
                code: 400, success: false, message: 'Duplicate User,please try other account', errors: [{ field: valueInvalid, message: `${valueInvalid} is invalid` }]
            }
            const hashedPassword = await argon2.hash(password)
            const newUser = User.create({
                username, password: hashedPassword,
                email,

            })
            const user = await User.save(newUser)
            return {

                code: 200, success: true, user: user, message: "Create User Success"

            }
            // return await  User.save(newUser)
        } catch (err) {

            return {
                code: 400, success: false, message: err
            }
        }

    }
    @Mutation(_returns => UserMutationResponse)
    async login(@Arg('loginInput') { usernameOrEmail, password }: LoginInput): Promise<UserMutationResponse> {
        try {
            const existingUser = await User.findOne(usernameOrEmail.includes('@') ? { email: usernameOrEmail } : { username: usernameOrEmail })
            if (!existingUser) {
                return {
                    code: 400, success: false, message: 'User not found', errors: [{
                        field:
                            'usernameOrEmail', message: 'Username or email not correct'
                    }]
                }
            }
            const passwordValid = await argon2.verify(existingUser.password, password)
            if (!passwordValid)
                return {
                    code: 400, success: false, message: 'Wrong password', errors: [{ field: 'password', message: 'WRONG PASSWORD' }]
                }
            return {
                code: 200, success: true, message: 'Login success', user: existingUser
            }
        } catch (error) {
            return {
                code: 400, success: false, message: error
            }
        }

    }
}